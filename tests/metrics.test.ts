import { afterAll, beforeAll, describe, expect, test } from 'vitest';
import { getInstance, ServiceUtils, teardown } from '../src/index.ts';
import { MetricTypeEnum } from '../src/metric/metric.ts';

describe( 'Metrics Test', () => {
	const metricsPort = 9001;
	const config = {
		service_name: 'Metrics Test',
		logging: {
			level: 'debug' as const
		},
		metrics: {
			type: 'prometheus' as const,
			port: metricsPort,
			name: 'Metrics Test',
			collectDefaultMetrics: false
		}
	};

	let serviceUtils: ServiceUtils;

	const fetchMetrics = () => fetch( `http://127.0.0.1:${ metricsPort }` )
		.then( ( res ) => res.text() );

	beforeAll( async () => {
		serviceUtils = await getInstance( { overrides: config } );
	} );

	afterAll( async () => {
		await teardown();
	} );

	test( 'serviceUtils.metrics.makeMetric', async () => {
		serviceUtils.metrics
			.makeMetric( {
				type: MetricTypeEnum.COUNTER,
				name: 'test_counter',
				prometheus: {
					name: 'Test counter 1',
					help: 'A counter using makeMetric'
				}
			} )
			.increment();

		const metrics = await fetchMetrics();
		expect( metrics ).toContain( 'Test_counter_1 1' );
	} );

	test( 'serviceUtils.metrics.makeRawMetric', async () => {
		serviceUtils.metrics
			.createMetric<MetricTypeEnum.COUNTER>( {
				type: MetricTypeEnum.COUNTER,
				name: 'Test counter 2',
				help: 'A counter using makeRawMetric'
			} )
			.inc();

		const metrics = await fetchMetrics();
		expect( metrics ).toContain( 'Test_counter_2 1' );
	} );

	test( 'makeMetric and makeRawMetric cross compatibility', async () => {
		serviceUtils.metrics
			.makeMetric( {
				type: MetricTypeEnum.COUNTER,
				name: 'test_counter_3',
				prometheus: {
					name: 'Test counter 3',
					help: 'A counter using both makeMetric and makeRawMetric'
				}
			} )
			.increment();

		serviceUtils.metrics
			.createMetric<MetricTypeEnum.COUNTER>( {
				type: MetricTypeEnum.COUNTER,
				name: 'Test counter 3',
				help: 'A counter using both makeMetric and makeRawMetric'
			} )
			.inc();

		const metrics = await fetchMetrics();
		expect( metrics ).toContain( 'Test_counter_3 2' );
	} );
} );
