import { describe, expect, test } from 'vitest';
import express from 'express';
import request from 'supertest';
import {
	TRACE_PARENT_HEADER,
	TRACE_STATE_HEADER
} from '@opentelemetry/core';
import { helpers } from '../src/index.ts';
import { X_REQUEST_HEADER } from '../src/trace.ts';

const traceParent = '00-0af7651916cd43dd8448eb211c80319c-b7ad6b7169203331-00';
const traceState = 'congo=t61rcWkgMzE';

describe( 'extractTraceContext', () => {
	const app = express();

	app.get( '/', async ( req, res ) => {
		const headers = helpers.extractTraceContext( req.headers as Record<string, string> );
		res.header( Object.fromEntries( headers.entries() ) );
		res.send( {} );
	} );

	test( 'extractTraceContext propagates headers', async () => {
		const res = await request( app ).get( '/' )
			.set( X_REQUEST_HEADER, 'test' )
			.set( TRACE_PARENT_HEADER, traceParent )
			.set( TRACE_STATE_HEADER, traceState );
		expect( res.headers[ X_REQUEST_HEADER ] ).toBe( 'test' );
		expect( res.headers.traceparent ).toEqual( traceParent );
		expect( res.headers.tracestate ).toEqual( traceState );
	} );
} );
