import { assert, afterAll, beforeAll, describe, expect, test } from 'vitest';
import { getInstance, ServiceUtils, teardown } from '../src/index.ts';
import { createWinstonLogger } from '../src/logging/index.ts';
import { wikimediaEcsFormatMiddleware } from '../src/logging/wikimedia_ecs.ts';
import fs from 'node:fs';
import { fileURLToPath } from 'url';
import path, { dirname } from 'path';

const __filename = fileURLToPath( import.meta.url );
const __dirname = dirname( __filename );

/**
 * We set the logger to write to a file, but we don't know
 * when the file is written. So we attempt 2 things:
 * 1. Enable retries
 * 2. Wait 1 second before attempting to read the file
 */
describe( 'Winston Test', () => {
	beforeAll( async () => {
		const config = {
			service_name: 'Winston Test',
			logging: {
				level: 'debug' as const,
				format: 'ecs' as const
			}
		};
		getInstance( { overrides: config } );
	} );

	afterAll( async () => {
		await teardown();
		fs.unlinkSync( path.join( __dirname, '..', 'createWinstonLogger.test' ) );
		fs.unlinkSync( path.join( __dirname, '..', 'createLogger.test' ) );
		fs.unlinkSync( path.join( __dirname, '..', 'errorStacktrace.test' ) );
		fs.unlinkSync( path.join( __dirname, '..', 'ecsFormatIntegrationTest.test' ) );
	} );

	const awaitTimeout = async ( func: () => any, ms: number = 500 ) => new Promise<void>( ( resolve ) => {
		setTimeout( () => {
			func();
			resolve();
		}, ms );
	} );

	test( 'ServiceUtils.createLogger', { retry: 3 }, async () => {
		const config = {
			service_name: 'ServiceUtils.createLogger test',
			logging: {
				level: 'debug' as const,
				format: 'simple' as const,
				transports: [
					{
						transport: 'File' as const,
						options: {
							filename: 'createLogger.test'
						}
					}
				]
			}
		};

		const logger = ServiceUtils.createLogger( config );
		logger.log( 'info', 'Test simple logger' );
		return await awaitTimeout( () => {
			const log = fs.readFileSync(
				path.join( __dirname, '..', 'createLogger.test' ),
				'utf8'
			);
			expect( log ).not.toContain( 'ecs.version' );
		} );
	} );

	test( createWinstonLogger, { retry: 3 }, async () => {
		const logger = createWinstonLogger( 'Simple logging test', {
			level: 'debug',
			format: 'simple',
			transports: [
				{
					transport: 'File',
					options: {
						filename: 'createWinstonLogger.test'
					}
				}
			]
		} );
		logger.debug( 'Test simple logger' );
		return await awaitTimeout( () => {
			const log = fs.readFileSync(
				path.join( __dirname, '..', 'createWinstonLogger.test' ),
				'utf8'
			);
			expect( log ).toContain(
				'debug: Test simple logger {"serviceName":"Simple logging test"}'
			);
		} );
	} );

	test( 'Error stacktrace', { retry: 3 }, async () => {
		const logger = createWinstonLogger( 'Error stacktrace test', {
			level: 'debug',
			format: 'simple',
			transports: [
				{
					transport: 'File',
					options: {
						filename: 'errorStacktrace.test'
					}
				}
			]
		} );
		assert.throws( () => {
			const err = new Error( 'test error' );
			logger.error( err );
			throw err;
		} );

		return await awaitTimeout( () => {
			const log = fs.readFileSync(
				path.join( __dirname, '..', 'errorStacktrace.test' ),
				'utf8'
			);
			expect( log ).toContain( '"stack":"Error: test error' );
		} );
	} );

	describe( 'WikimediaEcsFormatMiddleware', () => {
		test( 'Formats correctly', () => {
			const ecsOutput = {
				'@timestamp': '2025-01-10T21:42:16.231Z',
				'ecs.version': '8.10.0',
				'log.level': 'info',
				message: 'Incoming evaluator request',
				request: {
					headers: {
						'user-agent': 'kube-probe/1.23',
						'x-request-id': '258db3fa-d55c-4d72-a76d-8f93f944f9e2'
					},
					method: 'GET',
					params: {
						0: '/_info'
					},
					query: {},
					remoteAddress: '10.64.0.105',
					remotePort: 44150,
					url: '/_info'
				},
				requestId: '258db3fa-d55c-4d72-a76d-8f93f944f9e2',
				request_id: '258db3fa-d55c-4d72-a76d-8f93f944f9e2',
				service: 'function-evaluator'
			};

			const expected = {
				'@timestamp': '2025-01-10T21:42:16.231Z',
				'ecs.version': '8.10.0',
				'log.level': 'info',
				message: 'Incoming evaluator request',
				http: {
					request: {
						headers: {
							'user-agent': 'kube-probe/1.23',
							'x-request-id': '258db3fa-d55c-4d72-a76d-8f93f944f9e2'
						},
						method: 'GET',
						query: {},
						id: '258db3fa-d55c-4d72-a76d-8f93f944f9e2'
					}
				},
				'service.name': 'function-evaluator',
				source: {
					ip: '10.64.0.105',
					port: 44150
				},
				url: {
					full: '/_info',
					path: '/_info'
				}
			};

			const ecsFormatter = wikimediaEcsFormatMiddleware();
			const output = ecsFormatter.transform( {
				level: 'info',
				...ecsOutput
			} );
			expect( output ).toMatchObject( expected );
		} );

		test( 'Creates http.request.id if does not exist', () => {
			const ecsOutput = {
				'@timestamp': '2025-01-10T21:42:16.231Z',
				'ecs.version': '8.10.0',
				'log.level': 'info',
				message: 'Incoming evaluator request',
				requestId: '258db3fa-d55c-4d72-a76d-8f93f944f9e2',
				request_id: '258db3fa-d55c-4d72-a76d-8f93f944f9e2',
				service: 'function-evaluator'
			};

			const expected = {
				'@timestamp': '2025-01-10T21:42:16.231Z',
				'ecs.version': '8.10.0',
				'log.level': 'info',
				message: 'Incoming evaluator request',
				http: {
					request: {
						id: '258db3fa-d55c-4d72-a76d-8f93f944f9e2'
					}
				},
				'service.name': 'function-evaluator'
			};

			const ecsFormatter = wikimediaEcsFormatMiddleware();
			const output = ecsFormatter.transform( {
				level: 'info',
				...ecsOutput
			} );
			expect( output ).toMatchObject( expected );
		} );
	} );

	test( 'ECS Format Integration Test', { retry: 3 }, async () => {
		const logger = createWinstonLogger( 'ECS Format Integration Test', {
			level: 'debug',
			transports: [
				{
					transport: 'File',
					options: {
						filename: 'ecsFormatIntegrationTest.test'
					}
				}
			]
		} );

		logger.log( 'info', { message: 'ecsTest', requestId: '1234' } );
		return await awaitTimeout( () => {
			const log = fs.readFileSync(
				path.join( __dirname, '..', 'ecsFormatIntegrationTest.test' ),
				'utf8'
			);
			expect( JSON.parse( log ) ).toMatchObject( {
				'ecs.version': '8.10.0',
				'log.level': 'info',
				message: 'ecsTest',
				http: {
					request: {
						id: '1234'
					}
				},
				'service.name': 'ECS Format Integration Test'
			} );
		} );
	} );
} );
