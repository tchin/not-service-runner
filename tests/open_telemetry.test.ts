import { beforeEach, describe, expect, test } from 'vitest';
import {
	CompositePropagator,
	W3CTraceContextPropagator,
	TRACE_PARENT_HEADER,
	TRACE_STATE_HEADER,
	TraceState
} from '@opentelemetry/core';
import { helpers } from '../src/index.ts';
import { X_REQUEST_HEADER } from '../src/trace.ts';
import {
	defaultTextMapGetter,
	defaultTextMapSetter,
	propagation,
	ROOT_CONTEXT,
	SpanContext,
	trace,
	TraceFlags
} from '@opentelemetry/api';

const traceParent = '00-0af7651916cd43dd8448eb211c80319c-b7ad6b7169203331-00';
const traceState = 'congo=t61rcWkgMzE';
const spanContext: SpanContext = {
	traceId: '0af7651916cd43dd8448eb211c80319c',
	spanId: 'b7ad6b7169203331',
	traceFlags: TraceFlags.NONE,
	traceState: new TraceState( traceState )
};

describe( 'OpenTelemetry Tracing Tests', () => {
	let carrier: { [key: string]: unknown };

	beforeEach( () => {
		carrier = {};
	} );

	describe( 'XRequestPropagator', () => {
		const propagator = new helpers.XRequestPropagator();

		describe( '.inject()', () => {
			test( 'Injects generated x-request-id', () => {
				propagator.inject(
					trace.setSpan( ROOT_CONTEXT, trace.wrapSpanContext( spanContext ) ),
					carrier,
					defaultTextMapSetter
				);
				expect( carrier ).toHaveProperty( X_REQUEST_HEADER );
			} );
			test( 'Injects x-request-id from baggage', () => {
				propagator.inject(
					propagation.setBaggage(
						ROOT_CONTEXT,
						propagation.createBaggage( { [ X_REQUEST_HEADER ]: { value: 'test' } } )
					),
					carrier,
					defaultTextMapSetter
				);
				expect( carrier ).toHaveProperty( X_REQUEST_HEADER );
				expect( carrier[ X_REQUEST_HEADER ] ).toBe( 'test' );
			} );
		} );

		describe( '.extract()', () => {
			test( 'extracts x-request-id into baggage', () => {
				carrier = {
					[ X_REQUEST_HEADER ]: 'test'
				};

				const context = propagator.extract(
					ROOT_CONTEXT,
					carrier,
					defaultTextMapGetter
				);
				expect(
					propagation.getBaggage( context )?.getEntry( X_REQUEST_HEADER )?.value
				).toBe( 'test' );
			} );
		} );
	} );

	describe( 'XRequestPropagator W3CTraceContextPropagator Compatibility', () => {
		const propagator = new CompositePropagator( {
			propagators: [
				new W3CTraceContextPropagator(),
				new helpers.XRequestPropagator()
			]
		} );

		describe( '.inject()', () => {
			test( 'Injects x-request-id, traceparent, and tracestate', () => {
				propagator.inject(
					trace.setSpan( ROOT_CONTEXT, trace.wrapSpanContext( spanContext ) ),
					carrier,
					defaultTextMapSetter
				);
				expect( carrier ).toHaveProperty( X_REQUEST_HEADER );
				expect( carrier[ TRACE_PARENT_HEADER ] ).toEqual( traceParent );
				expect( carrier[ TRACE_STATE_HEADER ] ).toEqual( traceState );
			} );
		} );

		describe( '.extract()', () => {
			test( 'extracts x-request-id, traceparent, and tracestate', () => {
				carrier = {
					[ X_REQUEST_HEADER ]: 'test',
					[ TRACE_PARENT_HEADER ]: traceParent,
					[ TRACE_STATE_HEADER ]: 'congo=t61rcWkgMzE'
				};

				const context = propagator.extract(
					ROOT_CONTEXT,
					carrier,
					defaultTextMapGetter
				);
				expect(
					propagation.getBaggage( context )?.getEntry( X_REQUEST_HEADER )?.value
				).toBe( 'test' );
				expect( trace.getSpanContext( context )?.traceId ).toEqual(
					spanContext.traceId
				);
				expect( trace.getSpanContext( context )?.spanId ).toEqual(
					spanContext.spanId
				);
				expect( trace.getSpanContext( context )?.traceState?.get( 'congo' ) ).toEqual(
					spanContext.traceState?.get( 'congo' )
				);
			} );
		} );
	} );
} );
