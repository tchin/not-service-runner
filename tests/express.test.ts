import { afterEach, beforeEach, describe, expect, test } from 'vitest';
import request from 'supertest';
import express from 'express';
import { getInstance, teardown, helpers } from '../src/index.ts';
import type { ServiceUtils } from '../src/index.ts';

describe( 'Express Support Tests', () => {
	const metricsPort = 9001;
	const config = {
		service_name: 'Express Test',
		logging: {
			level: 'debug' as const
		},
		metrics: {
			type: 'prometheus' as const,
			port: metricsPort,
			collectDefaultMetrics: false
		}
	};

	let serviceUtils: ServiceUtils;

	beforeEach( async () => {
		serviceUtils = await getInstance( { overrides: config } );
	} );

	afterEach( async () => {
		await teardown();
	} );

	test( helpers.wrapExpressRouteHandlers, async () => {
		const app = express();
		app.get( '/', ( req, res ) => {
			res.send( 'Hello World!' );
		} );
		app.get( '/test', ( req, res ) => {
			res.send( 'Hello World!' );
		} );

		helpers.wrapExpressRouteHandlers( app, serviceUtils.metrics );

		await request( app ).get( '/' );
		await request( app ).get( '/test' );

		const metrics = ( await request( serviceUtils.prometheusServer.server ).get( '/' ) ).text;
		expect( metrics )
			.toContain( '{service="Express Test",path="root",method="GET",status="200"}' );
		expect( metrics )
			.toContain( '{service="Express Test",path="test",method="GET",status="200"}' );
	} );

	test( helpers.responseTimeMetricsMiddleware, async () => {
		const app = express();

		app.use( helpers.responseTimeMetricsMiddleware( serviceUtils ) );
		app.get( '/', ( req, res ) => {
			res.send( 'Hello World!' );
		} );
		app.get( '/test', ( req, res ) => {
			res.send( 'Hello World!' );
		} );

		await request( app ).get( '/' );
		await request( app ).get( '/test' );

		const metrics = ( await request( serviceUtils.prometheusServer.server ).get( '/' ) ).text;
		expect( metrics )
			.toContain( '{service="Express Test",path="/",method="GET",status="200"}' );
		expect( metrics )
			.toContain( '{service="Express Test",path="/test",method="GET",status="200"}' );
	} );

	test( 'helpers.responseTimeMetricsMiddleware legacyPathNaming', async () => {
		const app = express();

		app.use( helpers.responseTimeMetricsMiddleware( serviceUtils, true ) );
		app.get( '/', ( req, res ) => {
			res.send( 'Hello World!' );
		} );
		app.get( '/test/:id', ( req, res ) => {
			res.send( 'Hello World!' );
		} );

		await request( app ).get( '/' );
		await request( app ).get( '/test/1' );

		const metrics = ( await request( serviceUtils.prometheusServer.server ).get( '/' ) ).text;
		expect( metrics )
			.toContain( '{service="Express Test",path="root",method="GET",status="200"}' );
		expect( metrics )
			.toContain( '{service="Express Test",path="test/--id",method="GET",status="200"}' );
	} );
} );
