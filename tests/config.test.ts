import { afterAll, afterEach, beforeAll, describe, expect, test } from 'vitest';
import { ServiceUtils, getInstance, teardown } from '../src/index.ts';
import { fileURLToPath } from 'url';
import path, { dirname } from 'path';

const __filename = fileURLToPath( import.meta.url );
const __dirname = dirname( __filename );

describe( 'Config Test', () => {

	beforeAll( async () => {
		await getInstance();
	} );

	afterAll( async () => {
		await teardown();
	} );

	test( 'Config was read from file', async () => {
		expect(
			(
				await ServiceUtils.loadConfig( {
					cwd: path.join( __dirname, 'fixtures' )
				} )
			).service_name
		).toBe( 'Test Name' );
	} );

	test( 'Environment-specific config overrides', async () => {
		expect(
			(
				await ServiceUtils.loadConfig( {
					cwd: path.join( __dirname, 'fixtures' ),
					envName: 'development'
				} )
			).service_name
		).toBe( 'Test Name Development' );
	} );

	describe( 'Config was read from file via args', async () => {
		afterEach( () => {
			process.argv.pop();
			process.argv.pop();
		} );
		test( '-c args', async () => {
			process.argv.push( '-c', path.join( __dirname, 'fixtures', 'config' ) );
			expect( ( await ServiceUtils.loadConfig() ).service_name ).toBe(
				'Test Renamed Config'
			);
		} );
		test( '-d args', async () => {
			process.argv.push( '-d', path.join( __dirname, 'fixtures' ) );
			expect( ( await ServiceUtils.loadConfig() ).service_name ).toBe( 'Test Name' );
		} );
		test( '-f args', async () => {
			process.argv.push( '-d', path.join( __dirname, 'fixtures' ) );
			process.argv.push( '-f', 'config' );
			expect( ( await ServiceUtils.loadConfig() ).service_name ).toBe(
				'Test Renamed Config'
			);
		} );
	} );
} );
