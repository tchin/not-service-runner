import Prometheus from 'prom-client';
import type { MetricOptions } from './metric.ts';
import type { Metrics } from './index.ts';
import type { Logger } from 'winston';

/**
 * Normalizes a prometheus string. Should be used for label
 * and metric names, but is not needed for label values.
 *
 * @param {string} str
 * @return {string}
 */
export const normalize = ( str: string ) => String( str )
	.replace( /\W/g, '_' ) // replace non-alphanumerics
	.replace( /_+/g, '_' ) // dedupe underscores
	.replace( /(^_+|_+$)/g, '' ); // trim leading and trailing underscores

/**
 * @deprecated Use {@link Metrics.createMetric} to use Prometheus directly.
 *
 * Prometheus metrics implementation
 */
export class PrometheusMetric {
	options: MetricOptions;

	client: typeof Prometheus;

	staticLabels: any;

	metric: Prometheus.Metric;

	constructor( options: MetricOptions, client: typeof Prometheus ) {
		this.client = client;
		this.options = structuredClone( options );
		if ( this.options.labels === undefined ) {
			this.options.labels = { names: [] };
		}
		this.staticLabels = this.options.prometheus.staticLabels || {};

		// Add staticLabel names to list of known label names.
		Object.keys( this.staticLabels ).forEach( ( labelName ) => {
			this.options.labels?.names.unshift( labelName );
		} );
		// Normalize all the label names.
		this.options.labels.names = this.options.labels.names.map( normalize );

		this.options.prometheus.name = normalize(
			this.options.prometheus.name
		);
		this.metric = new this.client[ this.options.type ]( {
			name: this.options.prometheus.name,
			help: this.options.prometheus.help,
			labelNames: this.options.labels.names,
			buckets: this.options.prometheus.buckets,
			percentiles: this.options.prometheus.percentiles
		} );
	}

	/**
	 * Gets label values array for this metric
	 * including both static and dynamic labels merged together.
	 *
	 * @param {Array} labelValues
	 * @return {Array}
	 */
	_getLabelValues( labelValues: Array<string> ) {
		// make a clone of labelValues.
		const updatedLabelValues = [ ...labelValues ];
		// Add staticLabel values to updatedLabelValues if they aren't already listed.
		Object.keys( this.staticLabels ).forEach( ( labelName ) => {
			if ( !updatedLabelValues.includes( this.staticLabels[ labelName ] ) ) {
				updatedLabelValues.unshift( this.staticLabels[ labelName ] );
			}
		} );
		return updatedLabelValues;
	}

	increment( amount: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		// @ts-ignore
		this.metric.labels.apply( this.metric, updatedLabelValues ).inc( amount );
	}

	decrement( amount: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		// @ts-ignore
		this.metric.labels.apply( this.metric, updatedLabelValues ).dec( amount );
	}

	observe( value: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		// @ts-ignore
		this.metric.labels.apply( this.metric, updatedLabelValues ).observe( value );
	}

	gauge( amount: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		if ( amount < 0 ) {
			// @ts-ignore
			this.metric.labels
				.apply( this.metric, updatedLabelValues )
			// @ts-ignore
				.dec( Math.abs( amount ) );
		} else {
			// @ts-ignore
			this.metric.labels.apply( this.metric, updatedLabelValues ).inc( amount );
		}
	}

	set( value: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		// @ts-ignore
		this.metric.labels.apply( this.metric, updatedLabelValues ).set( value );
	}

	timing( value: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		this.observe( value, updatedLabelValues );
	}

	endTiming( startTime: number, labelValues: Array<string> ) {
		const updatedLabelValues = this._getLabelValues( labelValues );
		this.timing( ( Date.now() - startTime ) / 1000, updatedLabelValues );
	}
}

/**
 * @deprecated Use {@link Metrics.createMetric} to use Prometheus directly.
 */
export default class PrometheusClient {
	logger: Logger;

	client: typeof Prometheus;

	constructor( logger: Logger ) {
		this.logger = logger;
		this.client = Prometheus;
	}

	makeMetric( options: MetricOptions ) {
		return new PrometheusMetric( options, this.client );
	}

	close() {}
}
