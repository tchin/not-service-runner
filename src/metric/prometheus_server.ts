import Prometheus from 'prom-client';
import HTTP from 'http';

// This is used for Prometheus Client and Server
export interface PrometheusMetricsConfig {
	type: 'prometheus'; // TODO: Remove. Prometheus is the only supported type.
	port: number;
	name?: string; // TODO: Remove. Unused, from service-runner.
	collectDefaultMetrics?: boolean;
}

// Builds up an HTTP endpoint per Prometheus config
export default class PrometheusServer {
	config: PrometheusMetricsConfig;

	server: HTTP.Server;

	constructor( config: PrometheusMetricsConfig ) {
		this.config = config;
		if ( this.config.collectDefaultMetrics !== false ) {
			Prometheus.collectDefaultMetrics();
		}
		this.server = HTTP.createServer( async ( req, res ) => {
			res.end( await Prometheus.register.metrics() );
		} );
	}

	start() {
		this.server.listen( this.config.port );
	}

	async close() {
		Prometheus.register.clear();
		this.server.close( ( err ) => {
			Promise.resolve( err );
		} );
	}
}
