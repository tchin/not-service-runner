/**
 * @deprecated Legacy code from service-utils
 */
import type { Logger } from 'winston';
import type { Metrics, MetricType } from './index.ts';
import PrometheusClient, { PrometheusMetric } from './prometheus.ts';

// types correlate to Prometheus metric types.
export enum MetricTypeEnum {
	GAUGE = 'Gauge',
	HISTOGRAM = 'Histogram',
	COUNTER = 'Counter',
	SUMMARY = 'Summary',
	// eslint-disable-next-line @typescript-eslint/no-duplicate-enum-values
	TIMING = 'Histogram',
}

/**
 * @deprecated Use {@link Metrics.createMetric} to avoid using this interface.
 *
 * These options are for the specific metric being measured.
 *
 * @example
 * {
 *   type: 'Counter',
 *   name: 'hitcount',
 *   prometheus: {
 *     name: 'hitcount',
 *     help: 'hit count',
 *     staticLabels: {},  // a key-value pair of labels
 *     buckets: [], // https://github.com/siimon/prom-client#histogram
 *     percentiles: [], // https://github.com/siimon/prom-client#summary
 *   },
 *   sampleRate: 1, // default 1 https://github.com/brightcove/hot-shots/blob/v6.3.0/README.md#usage
 *   labels: {
 *     names: [],
 *     labelPosition: 'before',
 *     omitLabelNames: false
 *   }
 * }
 */
export interface MetricOptions {
	type: MetricType;
	/**
	 * Metrics are cached by this name, as opposed to makeRawMetrics
	 * where it's cached by the Prometheus metrics name
	 */
	name: string;
	prometheus: {
		name: string;
		help: string;
		staticLabels?: Record<string, string>;
		buckets?: Array<number>;
		percentiles?: Array<number>;
	};
	sampleRate?: number; // Default 1
	labels?: {
		names: Array<string>;
		labelPosition?: string;
		omitLabelNames?: boolean;
	};
}

/**
 * @deprecated Use {@link Metrics.createMetric} to use Prometheus directly.
 *
 * This class is a wrapper to call metrics functions
 */
export class Metric {
	type: MetricType;

	metric: PrometheusMetric;

	logger: Logger;

	constructor(
		client: PrometheusClient,
		logger: Logger,
		options: MetricOptions
	) {
		this.type = options.type;
		this.logger = logger;
		this.metric = client.makeMetric( options );
	}

	increment( amount = 1, labels: Array<string> = [] ) {
		if ( [ MetricTypeEnum.COUNTER, MetricTypeEnum.GAUGE ].includes( this.type as MetricTypeEnum ) ) {
			this.metric.increment( amount, labels );
		} else {
			this.logger.error(
				`increment() unsupported for metric type ${ this.type }`,
				{ levelPath: 'error/metrics' }
			);
		}
	}

	decrement( amount = 1, labels: Array<string> = [] ) {
		if ( this.type === MetricTypeEnum.GAUGE ) {
			this.metric.decrement( amount, labels );
		} else {
			this.logger.error(
				`decrement() unsupported for metric type ${ this.type }`,
				{ levelPath: 'error/metrics' }
			);
		}
	}

	observe( value: number, labels: Array<string> = [] ) {
		if ( [ MetricTypeEnum.HISTOGRAM, MetricTypeEnum.SUMMARY ].includes( this.type as MetricTypeEnum ) ) {
			this.metric.observe( value, labels );
		} else {
			this.logger.error( `observe() unsupported for metric type ${ this.type }`, {
				levelPath: 'error/metrics'
			} );
		}
	}

	gauge( amount: number, labels: Array<string> = [] ) {
		if ( this.type === MetricTypeEnum.GAUGE ) {
			this.metric.gauge( amount, labels );
		} else {
			this.logger.error(
				`set() or unique() unsupported for metric type ${ this.type }`,
				{ levelPath: 'error/metrics' }
			);
		}
	}

	set( value: number, labels: Array<string> = [] ) {
		if ( this.type === MetricTypeEnum.GAUGE ) {
			this.metric.set( value, labels );
		} else {
			this.logger.error(
				`set() or unique() unsupported for metric type ${ this.type }`,
				{ levelPath: 'error/metrics' }
			);
		}
	}

	timing( value: number, labels: Array<string> = [] ) {
		if ( this.type === MetricTypeEnum.TIMING ) {
			this.metric.timing( value, labels );
		} else {
			this.logger.error( `timing() unsupported for metric type ${ this.type }`, {
				levelPath: 'error/metrics'
			} );
		}
	}

	endTiming( startTime: number, labels: Array<string> = [] ) {
		if ( this.type === MetricTypeEnum.TIMING ) {
			this.metric.endTiming( startTime, labels );
		} else {
			this.logger.error(
				`endTiming() unsupported for metric type ${ this.type }`,
				{ levelPath: 'error/metrics' }
			);
		}
	}
}
