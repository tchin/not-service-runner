import type Prometheus from 'prom-client';
import PrometheusClient, { normalize } from './prometheus.ts';
import { Metric, type MetricOptions, type MetricTypeEnum } from './metric.ts';
import type { PrometheusMetricsConfig } from './prometheus_server.ts';
import type { Logger } from 'winston';

export type MetricType = MetricTypeEnum | `${ MetricTypeEnum }`;

type GenericPrometheusMetricsConfig<T extends MetricType> =
	ConstructorParameters<typeof Prometheus[T]>[0];

type RawMetricOptions<T extends MetricType> =
	GenericPrometheusMetricsConfig<T>
	& {
		type: T,
		[key: string]: any
	};

export class Metrics {
	options: PrometheusMetricsConfig | undefined;

	client: PrometheusClient;

	prometheus: typeof Prometheus;

	serviceName: string;

	cache: Map<string, Metric>;

	logger: Logger;

	constructor(
		serviceName: string,
		logger: Logger,
		options?: PrometheusMetricsConfig
	) {
		this.options = options;
		this.logger = logger;
		this.cache = new Map();
		this.serviceName = serviceName;
		if ( !options || options && options.type === 'prometheus' ) {
			this.client = new PrometheusClient(
				this.logger
			);
			this.prometheus = this.client.client;
			return;
		}
		logger.error(
			`No such metrics client: '${ options.type ?? 'undefined' }'`,
			{ levelPath: 'error/metrics' }
		);
	}

	getServiceName() {
		return this.serviceName;
	}

	// T247820: Selectively disable service label based on environment variable.
	// Intended for production use case where the service label will be set
	// by the Prometheus server.
	getServiceLabel() {
		if ( process.env.METRICS_SERVICE_LABEL_ENABLED === 'false' ) {
			return undefined;
		}
		return { service: this.serviceName };
	}

	fetchClient() {
		return this.client.constructor.name;
	}

	/**
	 * @deprecated Use {@link createMetric} for new projects.
	 *
	 * @param options
	 * @return
	 */
	makeMetric( options: MetricOptions ) {
		let metric = this.cache.get( options.name );
		if ( metric === undefined ) {
			metric = new Metric( this.client, this.logger, options );
			this.cache.set( options.name, metric );
		}
		return metric;
	}

	/**
	 * Makes and returns a raw Prometheus Metric bypassing the service-runner wrapper.
	 *
	 * @param options
	 * @return
	 */
	createMetric<T extends MetricType>(
		// Dynamically types the options to be the metrics options for Prometheus
		options: RawMetricOptions<T>
	): InstanceType<typeof Prometheus[T]> {
		const normalisedName = normalize( options.name );
		let metric = this.prometheus.register.getSingleMetric( normalisedName );
		if ( metric ) {
			return metric as InstanceType<typeof Prometheus[T]>;
		}

		let labelNames = [ normalisedName ];
		if ( options.labels?.names ) {
			labelNames = labelNames.concat( options.labels.names );
		}

		metric = new this.prometheus[ options.type ]( {
			name: normalisedName,
			help: options.help,
			labelNames,
			buckets: options.buckets,
			percentiles: options.percentiles
		} );

		this.prometheus.register.registerMetric( metric );
		return metric as InstanceType<typeof Prometheus[T]>;
	}

	close() {
		this.client.close();
	}
}
