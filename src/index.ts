import path from 'node:path';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { loadConfig } from 'c12';

import type winston from 'winston';
import {
	WinstonLoggerOptions,
	createWinstonLogger
} from './logging/index.ts';

import Prometheus from 'prom-client';
import { Metrics, type MetricType } from './metric/index.ts';
import type { MetricTypeEnum } from './metric/metric.ts';
import PrometheusServer, { PrometheusMetricsConfig } from './metric/prometheus_server.ts';

import { wrapRouteHandlers, responseTimeMetricsMiddleware } from './express.ts';
import { extractTraceContext } from './trace.ts';
import { XRequestPropagator } from './open_telemetry.ts';

const _yargs = ( ...args: Parameters<typeof yargs> ) =>
// Defaults that are described are infered from c12 and not a `default` property
	yargs( ...args )
		.usage( 'Usage: $0 [command] [options]' )
		.options( {
			'config-directory': {
				alias: 'd',
				describe: 'Absolute or relative path of the config file. Defaults to cwd',
				type: 'string',
				nargs: 1,
				global: true
			},
			'config-file': {
				alias: 'f',
				describe:
          'Name of the config file with or without the extension. Defaults to service-utils.config`',
				type: 'string',
				nargs: 1,
				global: true
			},
			config: {
				alias: 'c',
				describe:
          'Absolute or relative path of the config file with or without extension. Defaults to ${cwd}/service-utils.config',
				type: 'string',
				nargs: 1,
				global: true
			}
		} )
		.help( 'h' )
		.alias( 'h', 'help' );

// Call function at top level to support --help
// However, we will call this again in ServiceUtils.loadConfig to support tests
_yargs( hideBin( process.argv ) ).parseSync();

interface _Config {
	service_name?: string;
	logging?: WinstonLoggerOptions;
	metrics?: PrometheusMetricsConfig;
	[key: string]: any;
}

// Allow generic typing of arbitrary key/values
type Config<T extends Record<string, any>> = _Config & T;

export class ServiceUtils<T extends Record<string, any> = object> {
	config: Config<T>;

	logger: winston.Logger;

	metrics: Metrics;

	prometheusServer: PrometheusServer;

	/**
	 * Creates the logger, metrics, and prometheus metrics endpoint
	 *
	 * @param config
	 */
	constructor( config: Config<T> ) {
		this.config = config;
		this.logger = ServiceUtils.createLogger( config );

		// Backwards compatibility for array of metrics config
		if ( config.metrics instanceof Array ) {
			config.metrics = config.metrics[ 0 ];
		}

		if ( config.metrics ) {
			this.prometheusServer = ServiceUtils.createPrometheusServer(
				config.metrics as PrometheusMetricsConfig
			);
		}
		this.metrics = ServiceUtils.createMetrics( config, this.logger );
	}

	static createPrometheusServer( prometheus_config: PrometheusMetricsConfig ) {
		return new PrometheusServer( prometheus_config );
	}

	static createLogger( config: Config<Record<string, any>> ) {
		return createWinstonLogger(
			config.service_name ?? 'default_service',
			config.logging as WinstonLoggerOptions
		);
	}

	static createMetrics( config: Config<Record<string, any>>, logger: winston.Logger ) {
		return new Metrics(
			config.service_name ?? 'default_service',
			logger,
			config.metrics
		);
	}

	async teardown() {
		if ( this.prometheusServer ) {
			await this.prometheusServer.close();
		}
	}

	/**
	 * @param {Object=} options c12 loadConfig configuration
	 * @param {Object=} options.defaultConfig Default config overrideable from yaml
	 * @param {string=} options.envName Environment-conditional to load
	 * @param {string=} options.cwd Default config directory overrideable in cli
	 * @param {string=} options.configFile Name of the config file (without extension) overrideable in cli. Defaults to service-utils.config
	 * @return
	 */
	static async loadConfig<T extends Record<string, any>>(
		options?: Parameters<typeof loadConfig<Config<T>>>[0] & {
			useArgs?: boolean
		}
	) {
		const parsedArgs = _yargs( hideBin( process.argv ) ).parseSync();

		let configDirectory: string | undefined;
		let configFile: string | undefined;

		if ( options?.useArgs !== false ) {
			// -c arg
			if ( parsedArgs.config ) {
				const configPath = path.parse( parsedArgs.config );
				configDirectory = configPath.dir;
				configFile = configPath.name;
			}

			// TODO: Should these take precedence over -c?
			// -f arg
			configFile ??= parsedArgs.configFile ?? options?.configFile;
			// -d arg
			configDirectory ??= parsedArgs.configDirectory ?? options?.cwd;
			if ( configDirectory && !/^\//.test( configDirectory ) ) {
				// resolve relative paths
				configDirectory = path.resolve(
					`${ process.cwd() }/${ configDirectory ?? '' }`
				);
			}
		}

		const loadedConfig = await loadConfig<Config<T>>( {
			name: 'service-utils',
			...options,
			...{
				cwd: configDirectory,
				configFile: configFile
			}
		} );

		return loadedConfig.config as Config<T>;
	}
}

export { MetricType, MetricTypeEnum };
let singleton: ServiceUtils | undefined;

/**
 * @param {Object=} options c12 loadConfig configuration
 * @param {Object=} options.defaultConfig Default config overrideable from yaml
 * @param {string=} options.envName Environment-conditional to load
 * @param {string=} options.configDirectory Default config directory overrideable in cli
 * @param {string=} options.configFile Name of the config file (with or without extension) overrideable in cli. Defaults to service-utils.config
 * @param {false=} options.startPrometheusServer Starts Prometheus server if not set to false
 * @param {true=} options.useArgs Use yaml config fetched from cli args to override config if not set to false. Useful for testing.
 * @return {ServiceUtils}
 */
export const getInstance = async <T extends Record<string, unknown>>(
	options?: Parameters<typeof loadConfig<Config<T>>>[0] & {
		startPrometheusServer?: boolean;
		useArgs?: boolean;
	}
): Promise<ServiceUtils<T>> => {
	if ( singleton === undefined ) {
		const loadedConfig = await ServiceUtils.loadConfig<T>( options );
		singleton = new ServiceUtils<T>( loadedConfig );
		if ( options?.startPrometheusServer !== false && singleton.prometheusServer ) {
			singleton.prometheusServer.start();
		}
	}
	return singleton as ServiceUtils<T>;
};

export const teardown = async () => {
	if ( singleton !== undefined ) {
		await singleton.teardown();
		singleton = undefined;
	}
};

export const helpers = {
	wrapExpressRouteHandlers: wrapRouteHandlers,
	extractTraceContext,
	responseTimeMetricsMiddleware,
	XRequestPropagator
};

export const prometheus = Prometheus;
