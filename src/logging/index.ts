import winston from 'winston';
import { ecsFields, ecsStringify } from '@elastic/ecs-winston-format';
import { wikimediaEcsFormatMiddleware } from './wikimedia_ecs.ts';

export type Level = 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal';

/**
 * Configurable and serializable Winston logger options
 * to be derived into its associated objects with {@link createWinstonLogger}
 */
export interface WinstonLoggerOptions {
	level?: Level;
	stacktrace?: boolean;
	format?: keyof typeof winston.format | 'ecs';
	formatOptions?: any;
	transports?: Array<{
		transport: 'File' | 'Console' | 'Http' | 'Stream';
		options?: {
			format?: keyof typeof winston.format | 'ecs';
			[key: string]: any;
		};
	}>;
}

/**
 * @param serviceName Name of the service. Added to default metadata of the logger.
 * @param options {@link WinstonLoggerOptions}, not to be confused with {@link winston.LoggerOptions}
 * @return
 */
export const createWinstonLogger = (
	serviceName: string,
	options: WinstonLoggerOptions = {}
) => {
	// Uses spread operator to fill options
	// with defaults first and then override with config
	options = {
		...{
			level: 'info',
			stacktrace: true,
			format: 'ecs',
			transports: [ { transport: 'Console' } ]
		},
		...options
	};

	const ecsFormatter = [
		ecsFields( { convertReqRes: true, ...options.formatOptions } ),
		wikimediaEcsFormatMiddleware(),
		ecsStringify()
	];

	return winston.createLogger( {
		level: options.level,
		format: winston.format.combine(
			...[// Enable stacktrace for errors
				winston.format.errors( { stack: options.stacktrace } ),
				options.format === 'ecs' ?
					ecsFormatter :
					winston.format[ options.format ?? 'json' ](
						options.format ? options.formatOptions : undefined
					) ].flat()
		),
		defaultMeta: {
			serviceName: serviceName
		},
		transports: ( options.transports ?? [] ).map(
			( { transport, options: transportOptions } ) =>
			// @ts-ignore
				new winston.transports[ transport ]( {
					...transportOptions,
					// Spread format seperately so we can add on options as parameters
					...( transportOptions?.format ?
						{
							format: winston.format.combine(
								...[ winston.format.errors( { stack: options.stacktrace } ),
									transportOptions.format === 'ecs' ?
										ecsFormatter :
										winston.format[ transportOptions.format ](
											transportOptions.formatOptions
										) ].flat()
							)
						} :
						{} )
				} )

		)
	} );
};
