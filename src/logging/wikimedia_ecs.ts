// logform and triple-beam are transitive dependencies of winston
import type { TransformableInfo, Format } from 'logform';
import { LEVEL } from 'triple-beam';
// defu is a transitive dependency of c12
import { defu } from 'defu';

// (incomplete) ECS fragments derived from https://docs.wikimedia.org/ecs
// Only the keys used in the code are listed here.
interface Http {
	request?: {
		body?: {
			content?: unknown
		},
		id?: string,
		[key: string]: unknown
	},
	response?: {
		[key: string]: unknown
	},
	[key: string]: unknown
}

interface Url {
	full?: string,
	path?: string,
	query?: string,
	[key: string]: unknown
}

interface Source {
	ip?: string,
	port?: number,
	[key: string]: unknown
}

/**
 * A Winston `Format` for converting fields from the legacy bunyan structure
 * that was passed into Winston's ecs formatter into the standard fields
 * that Wikimedia uses.
 * Must be inserted betwwen ecsFields() and ecsStringify().
 *
 * @class {Format}
 * @param {Config} opts
 */
class WikimediaEcsFormatMiddleware implements Format {
	transform( info: TransformableInfo ) {
		if ( info.serviceName || info.service ) {
			info[ 'service.name' ] = info.serviceName ?? info.service;
			delete info.serviceName;
			delete info.service;
		}

		// See https://github.com/wikimedia/service-template-node/blob/main/lib/util.js#L45
		// for the expected structure we're adapting
		if ( info.request || info.response ) {
			const http: Http = {};
			const url: Url = {};
			const source: Source = {};

			if ( info.request ) {
				source.ip = info.request.remoteAddress;
				source.port = info.request.remotePort;
				url.full = info.request.url;
				// service-template-node dynamically defines routes
				// which are captured with params[n]
				url.path = info.request.params[ 0 ];
				delete info.request.remoteAddress;
				delete info.request.remotePort;
				delete info.request.url;
				delete info.request.params;

				http.request = info.request;
				delete info.request;
			}

			if ( info.response ) {
				http.response = info.response;
				delete info.response;
			}

			// Merge objects recursively (undefined values are ignored)
			info = defu( info, { http, url, source, message: info.message, level: info.level } );
		}

		if ( info.requestId || info.request_id ) {
			const http = { request: { id: info.requestId ?? info.request_id } };
			delete info.requestId;
			delete info.request_id;

			info = defu( info, { http, message: info.message, level: info.level } );
		}

		// Repair level from deep merge since it ignores symbols
		// TODO: SPLAT is also potentially mismatched, but doesn't seem to affect anything
		info[ LEVEL ] = info.level;

		return info;
	}
}

export const wikimediaEcsFormatMiddleware = () => new WikimediaEcsFormatMiddleware() as Format;
