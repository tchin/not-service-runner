import { v1 as uuidv1 } from 'uuid';

export const X_REQUEST_HEADER = 'x-request-id';

/**
 * Takes headers and extracts any trace headers (x-request-id, traceparent, tracestate)
 * and applies it to new headers.
 *
 * @param {HeadersInit} headers Headers to extract trace from
 * @return {Headers} new headers with trace headers set
 */
export const extractTraceContext = ( headers: HeadersInit ): Headers => {
	const _headers = new Headers( headers );
	const newHeaders = new Headers( {
		'x-request-id': _headers.get( 'x-request-id' ) ?? uuidv1()
	} );
	[ 'traceparent', 'tracestate' ].forEach( ( key ) => {
		const value = _headers.get( key );
		if ( value ) {
			newHeaders.set( key, value );
		}
	} );
	return newHeaders;
};
