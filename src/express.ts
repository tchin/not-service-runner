import type { NextFunction, Request as ExpressRequest, Response as ExpressResponse, Express } from 'express';
import type { Metrics } from './metric/index.ts';
import { MetricTypeEnum } from './metric/metric.ts';
import { type ServiceUtils } from './index.ts';

/**
 * @deprecated Use {@link responseTimeMetricsMiddleware} for new projects.
 *
 * Adapted from service-template-node.
 * https://github.com/wikimedia/service-template-node/blob/main/lib/util.js
 *
 * Awaits all of the given router's handler functions
 * inside a try/catch block so as to allow catching all errors,
 * regardless of whether a handler is async or not.
 *
 * @param {Express} app the Express application
 * @param {Metrics} metrics the metrics object
 */
export const wrapRouteHandlers = ( app: Express, metrics: Metrics ) => {
	app._router.stack
		.flatMap( ( routerLayer: any ) =>
		// TODO: Is this correct?
		// Ignore built-in routes (they have route:null in the stack)
			routerLayer.route ? routerLayer : []
		)
		.forEach( ( routerLayer: any ) => {
			const path = ( routerLayer.path ?? String( routerLayer.route.path.slice( 1 ) ) )
				.replace( /\/:/g, '/--' )
				.replace( /^\//, '' )
				.replace( /[/?]+$/, '' );

			routerLayer.route.stack.forEach( ( layer: any ) => {
				const origHandler = layer.handle;
				const metric = metrics.makeMetric( {
					type: MetricTypeEnum.HISTOGRAM,
					name: 'router',
					prometheus: {
						name: 'express_router_request_duration_seconds',
						help: 'request duration handled by router in seconds',
						staticLabels: metrics.getServiceLabel(),
						buckets: [ 0.01, 0.05, 0.1, 0.3, 1 ]
					},
					labels: {
						names: [ 'path', 'method', 'status' ],
						omitLabelNames: true
					}
				} );
				layer.handle = async (
					req: ExpressRequest,
					res: ExpressResponse,
					next: NextFunction
				) => {
					const startTime = Date.now();
					try {
						await origHandler( req, res, next );
					} catch {
						await next();
					} finally {
						let statusCode = res.statusCode || 500;
						if ( statusCode < 100 || statusCode > 599 ) {
							statusCode = 500;
						}
						metric.endTiming( startTime, [
							path || 'root',
							req.method,
							statusCode.toString()
						] );
					}
				};
			} );
		} );
};

/**
 * Adapted from cxserver
 * https://github.com/wikimedia/mediawiki-services-cxserver/blob/master/lib/util.js
 *
 * Wraps all routes in response time metrics.
 * The only behavioral change from service-template-node is
 * the original route is kept instead of normalized.
 *
 * @param {ServiceUtils} serviceUtils
 * @param {boolean} legacyPathNaming
 *   Whether to use the path naming conventions from service-template-node
 * @return
 */
export const responseTimeMetricsMiddleware = (
	serviceUtils: ServiceUtils, legacyPathNaming?: boolean
) => {
	// Create a histogram metric for HTTP request duration
	const requestDuration = {
		type: MetricTypeEnum.HISTOGRAM as const,
		name: 'express_router_request_duration_seconds',
		help: 'request duration handled by router in seconds',
		buckets: [ 0.01, 0.05, 0.1, 0.3, 1 ],
		labels: {
			names: [ 'service', 'path', 'method', 'status' ]
		}
	};
	// Create the metric
	// This will return the existing metric if it already exists
	const responseTimeMetric = serviceUtils
		.metrics.createMetric<MetricTypeEnum.HISTOGRAM>( requestDuration );
	serviceUtils.logger.info( 'responseTimeMetric', responseTimeMetric.labels );
	return ( req, res, next ) => {
		const endTimer = responseTimeMetric.startTimer();
		const originalEnd = res.end;

		res.end = ( ...args ) => {
			let path = req.route ? req.route.path : req.path;
			if ( legacyPathNaming ) {
				path = path
					.replace( /\/:/g, '/--' )
					.replace( /^\//, '' )
					.replace( /[/?]+$/, '' );
				path = path || 'root';
			}
			// Observe the duration
			endTimer( {
				service: serviceUtils.config.service_name,
				path: path,
				method: req.method,
				status: res.statusCode
			} );
			// Call the original end function
			originalEnd.apply( res, args );
		};
		// Continue processing the request
		next();
	};
};
