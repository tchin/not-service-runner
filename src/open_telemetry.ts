import {
	Context,
	propagation,
	TextMapGetter,
	TextMapPropagator,
	TextMapSetter
} from '@opentelemetry/api';
import { v1 as uuidv1 } from 'uuid';

import { X_REQUEST_HEADER } from './trace.ts';

/**
 * Propagates `x-request-id` headers. Used with OpenTelemetry.
 *
 * Do not use this in prduction until security review is finished
 * https://phabricator.wikimedia.org/T382349
 */
export class XRequestPropagator implements TextMapPropagator {

	/**
	 * Injects values from a given `Context` into a carrier.
	 *
	 * OpenTelemetry defines a common set of format values (TextMapPropagator),
	 * and each has an expected `carrier` type.
	 *
	 * @param {Context} context the Context from which to extract values to transmit over
	 *     the wire.
	 * @param {unknown} carrier the carrier of propagation fields, such as http request
	 *     headers.
	 * @param {TextMapSetter} setter an optional {@link TextMapSetter}. If undefined, values will be
	 *     set by direct object assignment.
	 */
	inject( context: Context, carrier: unknown, setter: TextMapSetter ): void {
		setter.set(
			carrier,
			X_REQUEST_HEADER,
			propagation.getBaggage( context )?.getEntry( X_REQUEST_HEADER )?.value ?? uuidv1()
		);
	}

	/**
	 * Given a `Context` and a carrier, extract context values from a
	 * carrier and return a new context, created from the old context, with the
	 * extracted values.
	 *
	 * @param {Context} context the Context from which to extract values to transmit over
	 *     the wire.
	 * @param {unknown} carrier the carrier of propagation fields, such as http request
	 *     headers.
	 * @param {TextMapGetter} getter an optional {@link TextMapGetter}. If undefined,
	 *     keys will be all own properties, and keys will be accessed by direct object access.
	 * @return {Context} context
	 */
	extract( context: Context, carrier: unknown, getter: TextMapGetter ): Context {
		const headers = getter.get( carrier, X_REQUEST_HEADER );
		const xRequestHeader = Array.isArray( headers ) ? headers[ 0 ] : headers;
		if ( !xRequestHeader ) {
			return context;
		}

		let baggage = propagation.getBaggage( context ) ?? propagation.createBaggage();
		baggage = baggage.setEntry( X_REQUEST_HEADER, { value: xRequestHeader } );

		return propagation.setBaggage( context, baggage );
	}

	fields(): string[] {
		return [ X_REQUEST_HEADER ];
	}
}
