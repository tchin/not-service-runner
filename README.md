# @wikimedia/service-utils

This is an incrementally-adoptable replacement library for [`service-runner`](https://github.com/wikimedia/service-runner/) that only does half the stuff it does (because we don't need the other stuff).

It aims to have a nearly identical API to `service-runner` while also exposing modern replacements for easy migration.

## What It Does

- Exposes configurable logging and metrics
- (Mostly) backwards-compatible interface of `service-runner`
- Partial replacement of `service-template-node` functionality
- ECS logging by default
- Header tracing with OpenTelemetry
- Works in ESM and CJS... with Typescript! In Node >= 20!

## What It Doesn't Do

- Master/Worker stuff
- Memory management
- Rate limiting
- Docker
- Auto-generate Open API Specs
- Statsd support
- I honestly don't know what all the stuff that `service-runner` does but it's safe to say that this doesn't do it

# Installation

`@wikimedia/service-utils` is hosted in GitLab's package registry underneath the `data-engineering` group.

To install, put the registry url into a `.npmrc` file at the top level of your project and then you can `npm install` as usual.

If you are building with Blubber, you might need to include the `.npmrc` file in the node builder to pick up the package ([example](https://gitlab.wikimedia.org/repos/data-engineering/eventstreams/-/blob/master/.pipeline/blubber.yaml?ref_type=heads#L15)).

```bash
echo @wikimedia:registry=https://gitlab.wikimedia.org/api/v4/groups/189/-/packages/npm/ >> .npmrc
npm i @wikimedia/service-utils
```

# Usage

`service-utils` exports a singleton through a `getInstance()` function. If you want to recreate the singleton (for example, with new config), you would need to call `teardown()` first.

```ts
import { getInstance, teardown } from "@wikimedia/service-utils";

const serviceUtils = await getInstance();

// Somewhere else in the service
serviceUtils.logger
  .log("info", "Test simple logger");

// See the #metrics section for a service-runner compatible api
serviceUtils.metrics
  .createMetric<MetricType.COUNTER>( {
    type: MetricType.COUNTER, // In Javascript it would be "Counter"
    name: 'test_counter',
    help: 'A counter'
  } )
  .inc();

// On service shutdown
await teardown();
```

## Configuration

This uses [`c12`](https://github.com/unjs/c12) to load and merge configuration.

Configuration is by default loaded at `${cwd}/service-utils.config.yaml`.

### Basic Configuration Example

```yaml
# service-utils.config.yaml
service_name: foobar

# Default config
logging:
  level: info
  format: ecs
  stacktrace: true
  transports:
    - transport: Console # Case matters

# Environment-specific variables, merged with defaults
# These can be arbitrarily named and applied on ServiceUtils intantiation with
# the `envName` option like: loadConfig({ envName: 'development' })
$development:
  service_name: foobar dev
  logging:
    level: verbose
    format: simple

$production:
  service_name: foobar prod
  logging:
    level: info
  metrics:
    port: 9001
```

### Migration from `service-runner`

This library supports `service-runner` configuration with minimal changes. It does *not* support Bunyan.

The only required change is a top level `service_name` key in your config file.

### ECS Logging

Winston is the logging library of `service-utils` as it supports ECS format. If you are migrating from a service that uses Bunyan, you will need to adapt your logging fields to conform to some [predefined keys](https://doc.wikimedia.org/ecs/).

```yaml
# service-utils.config.yaml
service_name: some_service

# Default config
logging:
  level: debug
  format: ecs
  stacktrace: true
  transports:
    - transport: Console # Case matters
```

### Metrics

`service-utils` exposes a `service-runner`-compatible metrics interface and config.
It only supports Prometheus and does *not* support statsd.

It also exposes a new `createMetric` function which dynamically types the metric options and returns the raw Prometheus Metrics.
This is recommended for new projects.

```yaml
# service-utils.config.yaml
service_name: some_service

metrics:
  port: 9001,
  collectDefaultMetrics: false # Defaults true
```

```ts
import { getInstance, helpers, MetricTypeEnum } from "@wikimedia/service-utils";

serviceUtils = await getInstance();

// Old way, service-runner compatible
serviceUtils.metrics
  .makeMetric( {
    type: MetricTypeEnum.COUNTER,
    name: 'test_counter',
    prometheus: {
      name: 'Test counter',
      help: 'A counter'
    }
  } )
  .increment();

// New way
serviceUtils.metrics
  .createMetric<MetricTypeEnum.COUNTER>( {
    type: MetricTypeEnum.COUNTER,
    name: 'test_counter',
    help: 'A counter'
  } )
  .inc(); // Be aware of the slight change in function names

await teardown();
```

If you don't want metrics at all, or want to initialize the server manually (like if you want it to listen on a route and not a separate port), remove the `metrics` config entirely or set `metrics: false`.
`service-utils` re-exports the base `prometheus` object for low level manipulation.

```ts
import express from 'express';
import { getInstance, teardown, helpers, prometheus } from '@wikimedia/service-utils';

const serviceUtils = await getInstance();
const app = express();

app.get( '/metrics', async (req, res) => {
	res.send(await prometheus.register.metrics());
} );

// service-utils uses the default Prometheus registry so this still works normally
serviceUtils.metrics
  .createMetric( {
    type: "Counter",
    name: 'test_counter',
    help: 'A counter'
  } )
  .inc();

```

In situations where you have both `service-runner` and `service-utils` pointing at the same config file, you can explicitly disable starting the Prometheus server to prevent clashing.

```ts
import { getInstance } from '@wikimedia/service-utils';

const serviceUtils = await getInstance({startPrometheusServer: false});
```

### CLI Args

You can override where to find the config file through the CLI.
```bash
node node_modules/@wikimedia/service-utils/dist/index.js --help
Usage: index.js [command] [options]

Options:
  --version           Show version number                          [boolean]
  -d, --config-directory  Absolute or relative path of the config file. Defaults to cwd [string]
  -f, --config-file       Name of the config file with or without the extension.
                          Defaults to service-utils.config`             [string]
  -c, --config            Absolute or relative path of the config file with or without the extension.
                          Defaults to ${cwd}/service-utils.config       [string]
  -h, --help              Show help                                    [boolean]
```

## Helpers
This library has helpers for common tasks.

### Express Integration

You can wrap Express routes in Prometheus histogram metrics to get request durations.

This library has a nearly identical implementation of `service-template-node`'s `wrapRouteHandlers` in the form of `helpers.wrapExpressRouteHandlers`.
Note that older services that use `service-template-node` might have different metric names and not use histogram but a gauge and you will have to adjust your dashboards accordingly.

A new more idiomatic implemntation in the form of an Express middleware is also provided with `helpers.responseTimeMetricsMiddleware`.
This has a slight behavioral change where the path label values are left untouched.
So `path="root"` and `path="test"` from the old wrapper becomes `path="/"` and `path="/test"`.
If you want the old way, set the `legacyPathNaming` to true.

```ts
import { getInstance, helpers } from "@wikimedia/service-utils";
import express from "express";

const serviceUtils = await getInstance();
const app = express();
const port = 3000;


// New way, done before any of the routes are registered.
app.use( helpers.responseTimeMetricsMiddleware( serviceUtils ) );
// If you want the old naming conventions from service-template-node
// app.use( helpers.responseTimeMetricsMiddleware( serviceUtils, true ) );

app.get("/", (req, res) => {
  res.send("Hello World!");
});
// ...

// Old way, done after all the routes are registered.
helpers.wrapExpressRouteHandlers( app, serviceUtils.metrics );

app.listen(port);
```

If you are initializing the Prometheus server manually, remember to put the metrics route *before* using `responseTimeMetricsMiddleware` or *after* using `wrapExpressRouteHandlers` so you don't accidentally create metrics for hitting the metrics route (unless you want that).

```ts
import express from 'express';
import { getInstance, teardown, helpers, prometheus } from '@wikimedia/service-utils';

const serviceUtils = await getInstance();
const app = express();

app.get( '/metrics', async (req, res) => {
	res.send(await prometheus.register.metrics());
} );

app.use( helpers.responseTimeMetricsMiddleware( serviceUtils ) );

app.get("/", (req, res) => {
  res.send("Hello World!");
});
// ...
```


### Trace Propagation

#### `x-request-id` OpenTelemetry Propagator
There is a custom OpenTelemetry propagator for `x-request-id` header tracing.
Use it by itself or with the W3 standardized tracing headers.

Under the hood it uses `Baggage` instead of `SpanContext` to propogate the header so it doesn't conflict with the W3 headers.
Let us know if this is undesireable.

Do not use this in prduction until [security review](https://phabricator.wikimedia.org/T382349) is finished.

```ts
import {
  CompositePropagator,
  W3CTraceContextPropagator,
} from '@opentelemetry/core';
import { api } from '@opentelemetry/api';
import { helpers } from '../src/index.ts';

api.propagation.setGlobalPropagator(
  new CompositePropagator({
    propagators: [
      new W3CTraceContextPropagator(),
      new helpers.XRequestPropagator()
    ]
  })
);
```

#### `extractTraceContext()`
There is a function to help propagate tracing headers when not
using OpenTelemetry.

```ts
import { helpers } from "@tchin/service-utils";
import express from "express";

const app = express();
app.get("/", (req, res) => {
  const headers = helpers.extractTraceContext(req.headers);
  res.send(await fetch(`some_url`, {headers}));
});

// ...
```


## Custom Config

You can add any key to `service-utils.config.yaml` and it will be available within the singleton's config property. You can type this custom config using the generic when getting the instance.

Given:
```yaml
# service-utils.config.yaml

service_name: some_service

foo: bar
```

You can type `ServiceUtils` with:

```ts
import { getInstance } from "@wikimedia/service-utils";

interface CustomConfig {
  foo: string;
}

const serviceUtils = await getInstance<CustomConfig>();
// This will be typed
console.log(serviceUtils.config.foo);
```

# Development

## Tips

- Variables are `camelCase` with the exception of variables that come from configuration which maintains their `snake_case` naming.
- There are a few unnecessary classes and interfaces. These are remnants of `server-runner` where it assumes it's handling multiple services at once. They will be removed sometime in the future.